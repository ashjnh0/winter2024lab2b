import java.util.Scanner;

public class gameLaunch {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Hi! You have two options of games to play");
		System.out.println("Enter 1 to play Hangman," + "\r\n" + "Enter 2 to play Wordle");
		
		int userInput = reader.nextInt();
		reader.nextLine();
		
		if (userInput == 1){
			System.out.println("Enter a 4 letter word:");
			String word = reader.nextLine().toUpperCase();
			System.out.print("\033[H\033[2J");
			boolean[] letters = new boolean[4];
			hangman.runGame(word, letters);
			hangman.printWork(word, letters);
		}
		else if (userInput == 2){
			String randomWord = wordle.generateWord();
			wordle.runGame(randomWord);
		}
		System.out.println("Pick 1 or 2 !!");
	}
}