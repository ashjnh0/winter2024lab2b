import java.util.Scanner;

public class hangman {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        // get user input
        System.out.println("Enter a 4 letter word:");
        String word = reader.next().toUpperCase();
        System.out.print("\033[H\033[2J");
        boolean[] letters = new boolean[4];
        // calls the printWork method with entered word and
        printWork(word, letters);
        runGame(word, letters);
    }

    public static int isLetterInWord(String word, char letter) {
        for (int i = 0; i < 4; i++) {
            if (word.charAt(i) == letter) {
                return i;
            }
        }
        return -1;
    }

    public static char toUpperCase(char c) {
        return Character.toUpperCase(c);
    }

    public static void printWork(String word, boolean[] letters) {
      String newWord = "";

      for (int i = 0; i < letters.length; i++) {
          if (letters[i]) {
              newWord += word.charAt(i);
          } else {
              newWord += " _ ";
          }
      }

      System.out.println("Your result is " + newWord);
    }

    public static void runGame(String word, boolean[] letters) {
      int numberOfGuesses = 0;

          while (numberOfGuesses < 6 && !(letters[0] && letters[1] && letters[2] && letters[3])) {
              Scanner reader = new Scanner(System.in);
              System.out.println("Guess a letter");
              char guess = reader.nextLine().toUpperCase().charAt(0);

              int letterIndex = isLetterInWord(word, guess);

              if (letterIndex >= 0) {
                  letters[letterIndex] = true;
              } else {
                  numberOfGuesses++;
              }

              System.out.println("Remaining guesses: " + (6 - numberOfGuesses));

              printWork(word, letters);
          }

          if (numberOfGuesses == 6) {
              System.out.println("Oops! Better luck next time :)");
          }

          if (letters[0] && letters[1] && letters[2] && letters[3]) {
              System.out.println("Congrats! You got it :)");
          }
    }
}