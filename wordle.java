import java.util.Scanner;
import java.util.Random;

public class wordle {
    //public static void main(String[] args) {
        //Scanner reader = new Scanner(System.in);
        //System.out.println("Let's Play Wordle!");
        //System.out.println("RULES: green is a correct letter, yellow if it's in the wrong place, gray if it's incorrect.");
        //String randomWord = generateWord();
        //runGame(randomWord);
    //}

    public static String readGuess() {
        Scanner scanner = new Scanner(System.in);
        String guess = "";

        while (guess.length() != 5) { //if user doesnt enter a 5 letter word it'll just ask them again to enter a 5 letter word
          System.out.print("Enter a 5 letter word: ");
          guess = scanner.nextLine().toUpperCase(); //takes user input and makes it uppercase
        }
      return guess;
    }

    public static String generateWord() {
        Random rando = new Random();
        String[] fiveLetterWord = {"peach", "grape", "lemon", "olive", "mango", "dates", "crazy", "angry", "fancy", "beach", "ocean", "stray", "heart", "blunt", "honey", "money", "price", "baker", "bread", "snack"};
        int randomIndex = rando.nextInt(fiveLetterWord.length); // random index
        return fiveLetterWord[randomIndex].toUpperCase(); // converts to uppercase
    }

    public static boolean letterInWord(String randomWord, char letter) {
        for (int i = 0; i < randomWord.length(); i++) {
            if (randomWord.charAt(i) == letter) { // checks if the letter is in the word
                return true;
            }
        }
        return false;
    }

    public static boolean letterInSlot(String word, char letter, int position) {
        if (position < 1 || position > word.length()) { //checks if the position is valid
            return false;
        }

        return word.charAt(position - 1) == letter;
    }

    public static String[] guessWord(String answer, String guess) {
        String[] colors = new String[answer.length()];

        for (int i = 0; i < answer.length(); i++) {
            char currentGuessChar = guess.charAt(i); //gets the current letter in the guess

            if (letterInSlot(answer, currentGuessChar, i + 1)) { //checks if the letter is in the right place
                colors[i] = "green";
            } else if (letterInWord(answer, currentGuessChar)) { 
                colors[i] = "yellow";
            } else {
                colors[i] = "white";
            }
        }

        return colors;
    }

    public static void presentResults(String word, String[] colors) {
        for (int i = 0; i < word.length(); i++) { 
            char letter = word.charAt(i);
            String colorCode;

            switch (colors[i]) {
                case "green":
                    colorCode = "\u001B[32m"; // Green
                    break;
                case "yellow":
                    colorCode = "\u001B[33m"; // Yellow
                    break;
                case "white":
                    colorCode = "\u001B[0m"; //reset to default (white)
                    break;
                default:
                    colorCode = ""; // default to no color
                    break;
            }

            System.out.print(colorCode + letter + "\u001B[0m" + " | ");
        }

        System.out.println("\u001B[0m"); 
    }
    public static void runGame(String word) {
        int maxAttempts = 6;
        int attempts = 0;
      
      for (attempts = 0; attempts < maxAttempts; attempts++){
          String guess = readGuess();
        
          String[] colors = guessWord(word, guess); // analyze the guess and get thecolors
          presentResults(guess, colors); //displays the results of the guess

          boolean isWin = true; // boolean to check if the player won
          for (int i = 0; i < colors.length; i++) {
            if (!colors[i].equals("green")) {
                isWin = false; // if any color is not green, the player has not won
              }
          }
          if (isWin) {
            System.out.println("YAYY! You win!");
            attempts = maxAttempts;  // loop stops when player wins
          }
      }
      if (attempts == maxAttempts) {
        System.out.println("You did not guess the word :( Try again!"); //if player doesnt guess the word in 6 attempts
    }
  }
}





